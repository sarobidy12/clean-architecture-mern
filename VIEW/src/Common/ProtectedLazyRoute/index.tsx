import { FC, memo, Suspense } from 'react';
import { Route, RouteProps } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

type AccessType = 'private' | 'public' | 'authentication';

export interface ProtectedLazyRouteProps extends RouteProps {
  noAccessRedirection?: string;
  fallbackComponent?: FC;
  access?: AccessType;
}

const defaultFallback = <CircularProgress />;

const ProtectedLazyRoute: FC<ProtectedLazyRouteProps> = memo(props => {
  
  const { component } = props;
  const FallbackComponent = defaultFallback;

  return (
    <Suspense fallback={FallbackComponent}>
      <Route {...props} component={component as any} />
    </Suspense>
  );

});

export default ProtectedLazyRoute;

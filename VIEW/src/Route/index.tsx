/* eslint-disable simple-import-sort/imports */
import { Home } from '@Page';
import { Switch } from 'react-router-dom';
import { ProtectedLazyRoute, Alert } from '@Common';
import { useAlert } from '@Hooks';

const Router = () => {

  const alert = useAlert();

  const handleClose = () => {
    alert.setCloseAlert();
  }

  return (
    <>
      <Switch>
        <ProtectedLazyRoute path="/" exact={true} component={Home} />
      </Switch>

      <Alert
        open={alert.open}
        handleClose={handleClose}
        severity={alert.severity}
        message={alert.message}
      />

    </>
  );
};

export default Router;

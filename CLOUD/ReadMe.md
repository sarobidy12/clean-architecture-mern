# How run Back end 

# with docker 

1. Build           :  docker-compose build
2. Start Back end  :  docker-compose up -d
2. Stop Back end   :  docker-compose stop
3. View Logs       :  docker-compose logs -f app
4. Install package :  docker-compose exec app sh

# with out Docker 

1. cd API/
2. yarn Or npm i
3. npm run dev


# If you want deploye you need compile ToJS

1. cd API/
2. npm run ToJs
3. Copy file in ./API/dist
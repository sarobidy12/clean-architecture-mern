db.auth('admin-user', 'admin-password');

db = db.getSiblingDB('cAdmin');

db.createUser({
  user: 'admin_usr',
  pwd: 'admin_321',
  roles: [
    {
      role: 'root',
      db: 'admin',
    },
  ],
});

db.adminCommand( { fsync: 1, lock: true } )
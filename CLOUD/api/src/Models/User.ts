import mongoose, { model, Schema, Model, Document } from 'mongoose';
 
export interface IUser extends Document {
    firstName: string;
    lastName: string;
    role: string;
}

// Create Schema

const UserSchema: Schema = new Schema({
    firstName: {
        type: String,
        require: true
    },
    lastName: {
        type: String,
        require: true
    },
    role:{
        type: String,
        require: true
    }
});

UserSchema.method('transform', function () {

    const obj = this.toObject();
    //Rename fields
    obj.id = obj._id;
    delete obj._id;

    return obj;

});

export const User: Model<IUser> = model("User", UserSchema) as any;

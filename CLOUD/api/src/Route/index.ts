import { Router } from "express";
import User from "./User";

const routes = Router();

routes.use("/", User); 

export default routes;

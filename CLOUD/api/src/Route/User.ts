import { Router } from "express";
import { UserController } from "@Controller";

const router = Router();

router.get(
    "/",
    UserController.createUser
);

export default router;

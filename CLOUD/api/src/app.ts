import express from 'express';
import cors from "cors";
import routes from "@Route";
import * as bodyParser from "body-parser";
import mongoose from "mongoose";
import helmet from "helmet";
import { config } from 'dotenv';

config();

const mongoURI: string = process.env.mongoDBURI || "";

const app = express();

console.log("mongoURI", mongoURI);

mongoose
  .connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("MongoDB Connected");
  })
  .catch((err) => console.log(err));

app.use(
  cors({
    exposedHeaders: ["token"],
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
  })
);

app.use(helmet());

app.use(bodyParser.json({ limit: "50mb" }));

// Set all routes from routes folder

app.use("/", routes);

const server = app.listen(process.env.PORT || 3009, () => {
  console.log(`Server started on port ${process.env.PORT || 3009}!`);
});

export default server;


